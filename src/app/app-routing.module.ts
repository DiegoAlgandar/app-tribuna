import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { IngresadoGuard } from './extras/ingresado.guard';
import { NoIngresadoGuard } from './extras/no-ingresado.guard';
import { RolGuard } from './extras/rol.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'Solicitar-equipo',
    pathMatch: 'full'
  },
  {
    path: 'Solicitar-equipo',
    loadChildren: () => import('./almacen/solicitar-equipo/solicitar-equipo.module').then( m => m.SolicitarEquipoPageModule),
    canActivate:[IngresadoGuard]
  },
  {
    path: 'Inventario',
    canActivate : [IngresadoGuard, RolGuard],
    children:[
      {
        path: '',
        loadChildren: () => import('./almacen/almacen.module').then( m => m.AlmacenPageModule),
      },
      {
        path: 'nuevo_equipo',
        loadChildren: () => import('./almacen/nuevo-equipo/nuevo-equipo.module').then( m => m.NuevoEquipoPageModule),
      },
      {
        path: 'detalle-equipo/:id',
        loadChildren: () => import('./detalle/detalle-equipo/detalle-equipo.module').then( m => m.DetalleEquipoPageModule),
      },
    ]   
  },
  {
    path: 'Acceder',
    loadChildren: () => import('./usuarios/login/login.module').then( m => m.LoginPageModule),
    canActivate: [NoIngresadoGuard]
  },
  {
    path: 'Registrarse',
    loadChildren: () => import('./usuarios/registro/registro.module').then( m => m.RegistroPageModule),
    canActivate: [NoIngresadoGuard]
  },
  {
    path: 'Empleados',
    loadChildren: () => import('./usuarios/empleados/empleados.module').then( m => m.EmpleadosPageModule),
    canActivate : [IngresadoGuard, RolGuard]
  },
  {
    path: 'Lista/Usuarios',
    loadChildren: () => import('./usuarios/listado/listado.module').then( m => m.ListadoPageModule),
    canActivate: [IngresadoGuard]
  },
  {
    path: 'Reportes',
    loadChildren: () => import('./incidencias/reporte/reporte.module').then( m => m.ReportePageModule),
    canActivate: [IngresadoGuard]
  },
  {
    path: 'Administracion',
    loadChildren: () => import('./usuarios/administracion/administracion.module').then( m => m.AdministracionPageModule),
    canActivate: [IngresadoGuard, RolGuard]
  },
  {
    path: 'Bitacora',
    loadChildren: () => import('./incidencias/bitacora/bitacora.module').then( m => m.BitacoraPageModule),
    canActivate: [IngresadoGuard, RolGuard]
  },
  {
    path: 'Generar_Codigo',
    loadChildren: () => import('./qr-modal/qr-modal.module').then( m => m.QrModalPageModule),
    canActivate: [IngresadoGuard]
  },
  {
    path: 'Devolver-equipo',
    loadChildren: () => import('./devolver-equipo/devolver-equipo.module').then( m => m.DevolverEquipoPageModule),
    canActivate: [IngresadoGuard]
  },
  {
    path: 'custom-modal',
    loadChildren: () => import('./custom-modal/custom-modal.module').then( m => m.CustomModalPageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
