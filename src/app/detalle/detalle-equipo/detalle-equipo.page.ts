import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import AxiosClient from 'src/app/extras/axiosClient';
import SwiperCore, { SwiperOptions, Pagination, EffectCoverflow } from 'swiper';
import { IonicSlides, ModalController, Platform } from '@ionic/angular';
import { SwiperComponent } from 'swiper/angular';
import { capitalize } from 'src/app/extras/helpers';
import { format, parseISO } from 'date-fns';
import { QrModalPage } from 'src/app/qr-modal/qr-modal.page';
import { EquiposService } from 'src/app/services/equipos.service';
import { environment } from 'src/environments/environment';
SwiperCore.use([EffectCoverflow, Pagination, IonicSlides]);
import 'swiper/css/effect-coverflow';
import 'swiper/css/pagination';
import { AlertaService } from 'src/app/services/alertas.service';

@Component({
  selector: 'app-detalle-equipo',
  templateUrl: './detalle-equipo.page.html',
  styleUrls: ['./detalle-equipo.page.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DetalleEquipoPage implements OnInit {
  @ViewChild(SwiperComponent) swiperSlideShow: SwiperComponent;
  plat: any;
  config: SwiperOptions = {};
  url = environment.SERVER_REF;
  constructor(
    private activatedRoute: ActivatedRoute,
    public platform: Platform,
    private modalController: ModalController,
    private equipoService: EquiposService, private alerta:AlertaService
  ) {}
  id;
  res;
  showSpinner: Boolean;
  IDCopiado: boolean;
  descargado: boolean;
  show = false;
  modalImagenes : Boolean = false
  Files: File[] = [];

  async ngOnInit() {
    this.modalImagenes = false
    this.descargado = false;
    this.IDCopiado = false;
    this.plat = await this.platform.platforms()[0];
    this.showSpinner = true;
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    await this.getData();
    if (this.res?.usuario_Actual != null) {
      await this.obtenerUsuarioActual(this.res.usuario_Actual);
    }
    this.config = {
      loop: false,
      pagination: true,
      effect: 'coverflow',
      speed: 1000,
    };
    console.log(this.res);
    this.Files = [];
  }
  async getData() {
    this.res = await this.equipoService.equipoInfo(this.id);
    this.showSpinner = false;
  }
  swipePrev() {
    this.swiperSlideShow.swiperRef.slidePrev();
  }
  swipeNext() {
    this.swiperSlideShow.swiperRef.slideNext();
  }
  convertToGigabytes(value) {
    if (value >= 1000) {
      return `${value / 1000} TB`;
    }
    return `${value} GB`;
  }
  async obtenerUsuarioActual(ID) {
    console.log(ID);
    const resp = await AxiosClient.get(`Empleados/Consulta/${ID}`);
    if (this.res.tipo != 'Chip') {
      this.res.usuario_Actual = {
        nombre: capitalize(resp.data.nombre),
      };
    }
  }
  formatDate(value: string) {
    return format(parseISO(value), 'dd/MM/yyyy p');
  }
  async copiarID(ID) {
    if (this.plat === 'desktop') {
      this.IDCopiado = true;
      setTimeout(() => {
        this.IDCopiado = false;
      }, 2000);
      return navigator.clipboard.writeText(ID);
    }
  }
  async generarQR() {
    const modal = await this.modalController.create({
      cssClass: 'popup-modal',
      component: QrModalPage,
      keyboardClose: false,
      backdropDismiss: false,
      id: 'Qr_Generado',
      componentProps: {
        data: { id: this.id, nombre: this.res.nombre_Equipo },
      },
    });
    return await modal.present();
  }
  capitalize(value) {
    return capitalize(value);
  }
  async modalImagenesToggle(){
    this.modalImagenes = !this.modalImagenes;
  }
  async onFileSelect(e) {
    this.Files = [];
    this.Files = await e.target.files;
  }
  async subir() {
    await this.equipoService.subir(this.Files, this.res.id);
  }
}
