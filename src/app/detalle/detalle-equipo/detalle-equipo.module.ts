import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalleEquipoPageRoutingModule } from './detalle-equipo-routing.module';

import { DetalleEquipoPage } from './detalle-equipo.page';
import { SwiperModule } from 'swiper/angular';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SwiperModule,
    DetalleEquipoPageRoutingModule
  ],
  declarations: [DetalleEquipoPage]
})
export class DetalleEquipoPageModule {}
