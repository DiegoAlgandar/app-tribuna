import { Component, OnInit } from '@angular/core';
import { ModalController, Platform } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import {  } from '../detalle/detalle-equipo/detalle-equipo.page';
import { capitalize } from '../extras/helpers';
import { EquiposService } from '../services/equipos.service';

@Component({
  selector: 'app-custom-modal',
  templateUrl: './custom-modal.page.html',
  styleUrls: ['./custom-modal.page.scss'],
})
export class CustomModalPage implements OnInit {
  constructor(
    private _modal: ModalController,
    private equipoService: EquiposService, private platform:Platform
  ) {}
  data;
  lista: any;
  plat
  url = environment.URL_SITIO
  async ngOnInit() {
    this.plat = await this.platform.platforms()[0]
    console.log(this.data);
    await this.obtenerLista();
  }
  async obtenerLista() {
    this.lista = await this.equipoService.obtenerEquiposLibres(this.data);
  }
  async seleccionar(equipo) {
    await this._modal.dismiss(equipo);
  }
  async cerrarModal() {
    return await this._modal.dismiss(null,null,'customModal');
  }
  async detalles(id) {
   await window.open(`${this.url}`+ `Inventario/detalle-equipo/${id}`)
  }
  capitalize(str){
    return capitalize(str)
  }
}
 