import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { capitalize } from '../../extras/helpers';
import { Platform } from '@ionic/angular';
import { AlertaService } from 'src/app/services/alertas.service';
import { SpinnerService } from 'src/app/services/spinner.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.page.html',
  styleUrls: ['./empleados.page.scss'],
})
export class EmpleadosPage implements OnInit {
  constructor(
    private alertCtrl: AlertaService,
    private spinnerCtrl: SpinnerService,
    private platform: Platform,
    public formBuilder: FormBuilder,
    private authService: AuthService
  ) {
  }
  empleadoForm: FormGroup;
  showForm: Boolean = false;
  lista: Array<any> = [];
  cargando: Boolean = false;
  token: any;
  page: number = 0;
  plat;
  nombre: string = ''
  ionViewWillEnter() {
    this.cargando = true;
    this.showForm = false;
  }
  async ngOnInit() {
    //var sesion = JSON.parse(localStorage.getItem('sesionActive'));
    //this.token = sesion.token;
    const loading = await this.spinnerCtrl.presentLoading();
    this.empleadoForm = this.formBuilder.group({
      Nombre: ['', [Validators.required]],
      Puesto: ['', [Validators.required]],
      Plaza: ['', [Validators.required]],
      Telefono: ['', [Validators.required]],
      Area: ['', [Validators.required]],
    });
    this.cargando = true;
    this.plat = await this.platform.platforms()[0];
    await this.obtenerEmpleados();
    await loading.dismiss();
    this.cargando = false;
    console.log(this.plat);
    
  }
  get errorControl() {
    return this.empleadoForm.controls;
  }
  capitalize(valor) {
    return capitalize(valor);
  }
  async nuevoEmpleado(e) {
    e.preventDefault();
    const loading = await this.spinnerCtrl.presentLoading();
    if (!this.empleadoForm.valid) {
      this.empleadoForm.markAllAsTouched();
      await loading.dismiss();
      return await this.alertCtrl.presentAlert(
        'Campos vacios',
        'Todos los campos son requeridos, llenelos y vuelva a enviar la peticion'
      );
    }
    //TokenAuth(this.token);
    var req = await this.authService.nuevoEmpleado(this.empleadoForm.value, loading);
    if (req) {
      await this.ngOnInit();
      this.limpiar()
    }
  }
  limpiar() {
    this.showForm = false;
    this.empleadoForm.markAsUntouched();
    this.empleadoForm.reset()
  }
  mostrarFormulario() {
    this.showForm = !this.showForm;
  }
  async obtenerEmpleados() {
    this.lista = await this.authService.obtenerEmpleados(this.page);
    console.log(this.lista);
  }
  async eliminar(ID) {
    await this.alertCtrl.presentAlert(
      '¿Deseas borrar este usuario?',
      '',
      'SI',
      async () => {
        await this.authService.eliminarEmpleado(ID);
        await this.ngOnInit();
      }
    );
  }
  async paginacion(type: string) {
    this.page = await this.authService.setPage(type, this.page);
    this.lista = await this.authService.obtenerEmpleados(this.page);
  }
  async busqueda(event) {
    this.page = 0;
    this.nombre = event.target.value;
    this.lista = await this.authService.burcarEmpleado(this.nombre);
  }
}