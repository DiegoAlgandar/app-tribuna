import { Component, OnInit } from '@angular/core';
import { MenuController, Platform } from '@ionic/angular';
import { AlertaService } from 'src/app/services/alertas.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { SpinnerService } from 'src/app/services/spinner.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  constructor(
    private menuCtrl: MenuController,
    private alertCtrl: AlertaService,
    private auth: AuthService,
    private sp: SpinnerService,
    public formBuilder: FormBuilder,
    private platform : Platform
  ) {
  }
  validation;
  loginForm: FormGroup;
  plat
  ionViewWillEnter() {
    this.menuCtrl.enable(false);
    this.limpiar();
  }
  async ngOnInit() {
    this.loginForm = this.formBuilder.group({
      NombreUsuario: ['', [Validators.required]],
      Contrasenia: ['', [Validators.required]],
    });
    this.plat = await this.platform.platforms()[0];
  }
  get errorControl() {
    return this.loginForm.controls;
  }
  async acceder(e) {
    e.preventDefault();
    const loading = await this.sp.presentLoading();
    if(!this.loginForm.valid) {
      this.loginForm.markAllAsTouched();
      await loading.dismiss();
    }
    const {NombreUsuario,Contrasenia} = this.loginForm.value
    if (!NombreUsuario || !Contrasenia) {
      await loading.dismiss();
      return await this.alertCtrl.presentAlert(
        'Todos los campos son obligatorios',
        'Uno de los campos no a sido proporcionado, por favor vuelva a intentarlo'
      );
    }
    let LoginObj = {
      NombreUsuario,
      Contraseña: Contrasenia,
    };
    var res = await this.auth.login(LoginObj,loading);
    if (res) {
      this.limpiar();
    }
  }
  limpiar() {
    this.loginForm.reset()
    this.loginForm.markAsUntouched()
  }
}
