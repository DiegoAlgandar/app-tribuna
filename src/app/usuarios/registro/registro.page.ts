import { Component, OnInit } from '@angular/core';
import { MenuController, ModalController, Platform } from '@ionic/angular';
import { AlertaService } from 'src/app/services/alertas.service';
import { SpinnerService } from 'src/app/services/spinner.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { confirmedValidator } from 'src/app/extras/helpers';
import { AuthService } from 'src/app/services/auth.service';
import { ListadoPage } from '../listado/listado.page';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  constructor(
    private menuCtrl: MenuController, private alertaService: AlertaService,
    private service: SpinnerService, public formBuilder: FormBuilder,
    private authService: AuthService, private platfotm : Platform,
    private modal : ModalController
  ) { }
  confirmacion: string = '';
  ionicForm: FormGroup;
  plat
  empleado=null
  ionViewWillEnter() {
    this.menuCtrl.enable(false);
    this.ionicForm.markAsUntouched()
    this.limpearFormulario()
  }
  async ngOnInit() {
    this.empleado=null
    this.ionicForm = this.formBuilder.group(
      {
        NombreUsuario: ['', [Validators.required]],
        Nombre: ['', [Validators.required]],
        Apellido_Paterno: ['', [Validators.required]],
        Apellido_Materno: ['', [Validators.required]],
        Contrasenia: ['', [Validators.required, Validators.minLength(8)]],
        Contrasenia_Confirmacion: ['', [Validators.required]],
        Rol: ['', [Validators.required]],
      },
      {
        validator: confirmedValidator(
          'Contrasenia',
          'Contrasenia_Confirmacion'
        ),
      }
    );
    this.plat = await this.platfotm.platforms()[0]
  }
  get errorControl() {
    return this.ionicForm.controls;
  }
  limpearFormulario() {
    this.ionicForm.reset('')
    this.empleado = null
  }
  async registrarse(e) {
    e.preventDefault();
    const loading = await this.service.presentLoading();
    if (!this.ionicForm.valid || !this.empleado) {
      await loading.dismiss();
      this.ionicForm.markAllAsTouched();
      return await this.alertaService.presentAlert(
        'Todos los campos son obligatorios',
        '¡Porfavor, llene todos los campos y vuelva a registrarse!'
      );
    }
    const { Nombre, NombreUsuario, Contrasenia,
      Rol, Apellido_Materno, Apellido_Paterno } = this.ionicForm.value
    var body = {
      Nombre,
      NombreUsuario,
      Contraseña: Contrasenia,
      Rol,
      Autorizado: false,
      Apellido_Materno,
      Apellido_Paterno,
      EmpleadoID: this.empleado.ID
    };
    var req = await this.authService.nuevoUsuario(body, loading)
    if (req) {
      this.limpearFormulario()
    }
  }
  async seleccionarEmpleado(){
    this.empleado=null
    var modal = await this.modal.create({
      component: ListadoPage,
      keyboardClose: false,
      backdropDismiss: false,
      id: 'Listado'
    });
    modal.onDidDismiss().then((x) => {
      if (x.data != undefined || x.data != null) {
        this.empleado = x.data
      }
      return
    })
    return await modal.present()
  }
}
