import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import AxiosClient from 'src/app/extras/axiosClient';
import { capitalize } from 'src/app/extras/helpers';
import { AlertaService } from 'src/app/services/alertas.service';
import { TOKEN_KEY } from 'src/app/services/auth.service';
import { SpinnerService } from 'src/app/services/spinner.service';

@Component({
  selector: 'app-administracion',
  templateUrl: './administracion.page.html',
  styleUrls: ['./administracion.page.scss'],
})
export class AdministracionPage implements OnInit {
  constructor(
    private spinner: SpinnerService,
    private alertCtrl: AlertaService,
    private platform: Platform
  ) { }
  registros: any = [];
  plat: any;
  mostrarTodos: boolean;
  cargando: boolean = false;
  ionViewWillEnter() {
    this.cargando = true;
  }
  async ngOnInit() {
    this.cargando = true
    const loading = await this.spinner.presentLoading();
    this.mostrarTodos = false;
    this.plat = await this.platform.platforms()[0];
    console.log(this.plat);
    const sesion = JSON.parse(localStorage.getItem(TOKEN_KEY));
    console.log(sesion.rol);
    if (sesion) {
      await this.obtenerRegistros();
    }
    await loading.dismiss();
    this.cargando = false
  }
  async obtenerRegistros() {
    try {
      const res = await AxiosClient.get('Usuarios/Registros_Pendientes');
      console.log(res.data);
      this.registros = res.data;
    } catch (error) {
      console.log(error.message);
      await this.alertCtrl.presentToast(error.message)
    }
  }
  async recargar(e?) {
    await this.ngOnInit();
    if (e) {
      e.target.complete();
    }
  }
  async confirmarRegistro(registro) {
    await this.alertCtrl.presentAlert(
      '¿Seguro que desea dar acceso a este usuario?',
      '',
      'SI',
      async () => {
        const res = await AxiosClient.put(
          'Usuarios/ConfirmarRegistro',
          registro.id,
          { headers: { 'Content-Type': 'application/json' } }
        );
        console.log(res.data);
        this.recargar();
      }
    );
  }
  async obtenerTodos() {
    if (this.mostrarTodos) {
      const res = await AxiosClient.get('Usuarios/Confirmados');
      console.log(res.data);
      this.registros = res.data;
    } else {
      await this.obtenerRegistros();
    }
  }
  async eliminarUsuario(ID) {
    const res = await AxiosClient.delete(`/Usuarios/EliminarUsuario/${ID}`);
    console.log(res.data);
  }
  formatearNombre(registro) {
    const { nombre, apellido_Paterno, apellido_Materno } = registro;
    const nombreCompleto = `${nombre} ${apellido_Paterno} ${apellido_Materno}`;
    return capitalize(nombreCompleto);
  }
  CapitalizarTexto(texto) {
    return capitalize(texto);
  }
}
