import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import AxiosClient from 'src/app/extras/axiosClient';
import { capitalize } from 'src/app/extras/helpers';

@Component({
  selector: 'app-historial',
  templateUrl: './listado.page.html',
  styleUrls: ['./listado.page.scss'],
})
export class ListadoPage implements OnInit {
  constructor(private modal: ModalController) {}
  lista: Array<any> = [];
  nombre:string=null
  listaFiltrada : Array<any> = []

  async ngOnInit() {
    await this.obtenerUsuarios();
    this.listaFiltrada = this.lista
  }
  
  async cerrarModal(obj?) {
    await this.modal.dismiss(obj, null, 'Listado');
  }
  async seleccionar(empleado) {
    var obj = {
      ID: empleado.id,
      Nombre: capitalize(empleado.nombre)
    }
    await this.cerrarModal(obj);
  }
  async obtenerUsuarios() {
    const res = await AxiosClient.get('Empleados/Todos');
    this.lista = res.data;
  }
  onSearch(){    
    this.listaFiltrada = this.lista.filter((x)=>x.nombre.toLowerCase().includes(
      this.nombre.toLowerCase().trim()
    ))
  }
  formatearNombre(value){
    return capitalize(value)
  }
}
