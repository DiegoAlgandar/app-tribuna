import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BitacoraPageRoutingModule } from './bitacora-routing.module';

import { BitacoraPage } from './bitacora.page';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import {NgxPrintModule} from 'ngx-print';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgxPrintModule,
    NgxQRCodeModule,
    BitacoraPageRoutingModule
  ],
  declarations: [BitacoraPage]
})
export class BitacoraPageModule {}
