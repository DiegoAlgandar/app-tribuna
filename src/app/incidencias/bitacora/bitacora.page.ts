import { Component, OnInit } from '@angular/core';
import { MenuController, Platform } from '@ionic/angular';
import { format, parseISO } from 'date-fns';
import AxiosClient from 'src/app/extras/axiosClient';
import { capitalize, CATEGORIAS, fecha } from 'src/app/extras/helpers';
import { Bitacora } from 'src/app/models/Bitacora';
import { ResolverIncidencia } from 'src/app/models/RespuestaIncidencia';
import { AlertaService } from 'src/app/services/alertas.service';
import { TOKEN_KEY } from 'src/app/services/auth.service';
import { SpinnerService } from 'src/app/services/spinner.service';

@Component({
  selector: 'app-bitacora',
  templateUrl: './bitacora.page.html',
  styleUrls: ['./bitacora.page.scss'],
})
export class BitacoraPage implements OnInit {
  constructor(
    private spinner: SpinnerService,
    private platform: Platform,
    private alertCtrl: AlertaService,
    private menu: MenuController
  ) {}
  cargando: Boolean = false;
  tiempo_Invertido;
  bitacora: Array<Bitacora> = [];
  respIncidencia: ResolverIncidencia;
  sesionID;
  mostrarResueltas: boolean;
  categorias = CATEGORIAS;
  plat;
  btn_Ocultar: boolean;
  fechaActual : Date= new Date()
  ionViewWillEnter() {
    this.menu.enable(true);
  }
  async ngOnInit() {
    this.cargando = true;
    const loading = await this.spinner.presentLoading();
    this.plat = await this.platform.platforms()[0];
    this.btn_Ocultar = false;
    this.mostrarResueltas = false;
    this.respIncidencia = new ResolverIncidencia();
    const sesion = await JSON.parse(localStorage.getItem(TOKEN_KEY));
    await this.bitacora_Pendientes();
    this.sesionID = sesion.id;
    await loading.dismiss();
    console.log(this.bitacora);
    this.cargando = false;
  }
  async bitacora_Pendientes() {
    try {
      const res = await AxiosClient.get('Incidencias');
      for (let index = 0; index < res.data.length; index++) {
        const element = res.data[index];
        element.solicitante = await this.infoUsuario(element.usuarioID);
      }
      this.bitacora = res.data;
      console.log(res.data);
    } catch (error) {
      console.log(error.message);
      await this.alertCtrl.presentToast(error.message);
    }
  }
  async bitacora_Resueltas() {
    this.btn_Ocultar = false;
    try {
      if (this.mostrarResueltas) {
        const res = await AxiosClient.get('Incidencias/Resueltas');
        for (let index = 0; index < res.data.length; index++) {
          const element = res.data[index];
          element.tiempo_Invertido = element.tiempo_Invertido + ` ${element.tiempo_Invertido>1? 'horas':'hora'}`;
          element.solicitante = await this.infoUsuario(element.usuarioID);
          if (element.estado === 'Cerrado') {
            element.nombreResponsable = await this.infoUsuario(
              element.responsable
            );
          }
        }
        this.bitacora = res.data;
        console.log(this.bitacora);
        
      } else {
        await this.bitacora_Pendientes();
      }
    } catch (error) {
      console.log(error.message);
    }
  }
  async ResolverIncidencia(bit, i?) {
    if (this.plat != 'desktop') {
      var btnMobile = await this.info(i);
      btnMobile.classList.toggle('contraer');
    } else {
      var btnDesktop = await this.btnInfo(i);
      btnDesktop.textContent === 'Expandir'
        ? (btnDesktop.innerHTML = 'Ocultar')
        : (btnDesktop.innerHTML = 'Expandir');
    }
    let text = await document.getElementById(`span${bit.id}`);
    text.classList.toggle('problema');
    let form = await document.getElementById(bit.id);
    if (form.style.display === 'none') {
      form.style.display = 'block';
      form.style.transition = 'all .3 ease';
      form.style.height = '200px';
    } else {
      form.style.display = 'none';
    }
  }
  async onSubmit(e, obj) {
    e.preventDefault();
    this.respIncidencia.IdRef = obj.id;
    this.respIncidencia.Responsable = this.sesionID;
    console.log(this.respIncidencia);
    
    if (this.respIncidencia.Solucion.trim()=="" || this.respIncidencia.Categoria=="" || !this.tiempo_Invertido) {
      return await this.alertCtrl.presentToast('Todos los campos son requeridos')
    }

    if (this.tiempo_Invertido.toString().includes(' ')) {
      var x = this.tiempo_Invertido.toString().split(' ');
      var tiempo = parseInt(x[0]);
      if (x[1].toLowerCase().includes('min')) {
        this.respIncidencia.Tiempo_Invertido = tiempo / 60; //convertido a minutos
      }
      if (x[1].toLowerCase().includes('h')) {
        this.respIncidencia.Tiempo_Invertido = tiempo;
      }
    } else {
      return await this.alertCtrl.presentToast(
        'Especifique el tiempo invertido (horas o minutos)'
      );
    }
    try {
      var res = await AxiosClient.put(
        'Incidencias/Resolver',
        this.respIncidencia,
        { headers: { 'Content-Type': 'application/json' } }
      );
      console.log(res.data);
      this.limpiar();
      this.ngOnInit();
    } catch (error) {
      console.log(error);
    }
  }
  formatoFecha(value: string) {
    return format(parseISO(value), 'dd/MM/yyyy');
  }
  async infoUsuario(ID) {
    try {
      var res = await AxiosClient.get(`Usuarios/Consulta/${ID}`);
    const { nombre, apellido_Paterno } = res.data;
    var nombreFormateado = `${nombre?.split(' ')[0]} ${apellido_Paterno}`;
    return nombreFormateado;
    } catch (error) {
      console.log(error);
    }
  }
  capitalize(value) {
    return capitalize(value);
  }
  setPrioridad(prioridad) {
    if (prioridad === 'Alta') {
      return 'danger';
    }
    if (prioridad === 'Mediana') {
      return 'warning';
    }
    if (prioridad === 'Baja') {
      return 'primary';
    }
  }
  limpiar() {
    this.tiempo_Invertido = null;
    this.respIncidencia = new ResolverIncidencia();
  }
  async info(i) {
    return await document.getElementById('expandir' + i);
  }
  async btnInfo(i) {
    return await document.getElementById('btnToggle' + i);
  }
  fecha(){
    return fecha(this.fechaActual)
  }
}
