import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import AxiosClient from 'src/app/extras/axiosClient';
import { AlertaService } from 'src/app/services/alertas.service';
import { TOKEN_KEY } from 'src/app/services/auth.service';
import { SpinnerService } from 'src/app/services/spinner.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AREAS, PLAZAS } from 'src/app/extras/helpers';

@Component({
  selector: 'app-reporte',
  templateUrl: './reporte.page.html',
  styleUrls: ['./reporte.page.scss'],
})
export class ReportePage implements OnInit {
  constructor(
    private platform: Platform,
    private alertCtrl: AlertaService,
    private spinner: SpinnerService,
    public formBuilder: FormBuilder
  ) {}
  id;
  plat;
  reporteForm: FormGroup;
  plazas = PLAZAS;
  areas = AREAS;
  reporteEnviado: boolean;

  ionViewWillEnter() {
    this.limpiar();
  }
  async ngOnInit() {
    this.reporteForm = this.formBuilder.group({
      Problema: ['', [Validators.required]],
      Prioridad: ['', [Validators.required]],
      Plaza: ['', [Validators.required]],
      Area: ['', [Validators.required]],
      Tipo: ['', [Validators.required]],
    });
    this.plat = this.platform.platforms()[0];
    const sesion = await JSON.parse(localStorage.getItem(TOKEN_KEY));
    this.id = sesion.id;
    this.reporteEnviado = false;
  }
  get errorControl() {
    return this.reporteForm.controls;
  }
  async onSubmit(e) {
    e.preventDefault();
    const loading = await this.spinner.presentLoading();
    if (!this.reporteForm.valid) {
      this.reporteForm.markAllAsTouched();
      await loading.dismiss();
      return await this.alertCtrl.presentAlert(
        'Campos Vacios',
        'Todos los campos son requeridos, llenelos y vuelva a enviar la peticion'
      );
    }
    const { Problema, Prioridad, Plaza, Area, Tipo } = this.reporteForm.value;
    let body = {
      Problema,
      Prioridad,
      Area,
      Plaza,
      Tipo,
      UsuarioID: this.id,
      Estado: 'Abierto',
    };
    try {
      const res = await AxiosClient.post('Incidencias', body, {
        headers: { 'Content-Type': 'application/json' },
      });
      if ((res.data = 1)) {
        this.reporteEnviado = true;
        this.limpiar();
        await this.alertCtrl.presentToast('Reporte enviado con éxito');
      }
    } catch (error) {
      return await this.alertCtrl.presentToast(
        'Ha ocurrido un error enviando su reporte, intentelo de nuevo.'
      );
    }
    await loading.dismiss();
  }
  limpiar() {
    this.reporteForm.markAsUntouched();
    this.reporteForm.reset();
  }
  otroReporte() {
    this.reporteEnviado = false;
  }
}
