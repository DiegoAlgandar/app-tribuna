import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { capitalize } from '../extras/helpers';
import { AlertaService } from '../services/alertas.service';
import { AuthService } from '../services/auth.service';
import { EquiposService } from '../services/equipos.service';
import { PhotoService } from '../services/photo.service';
import { SpinnerService } from '../services/spinner.service';

@Component({
  selector: 'app-devolver-equipo',
  templateUrl: './devolver-equipo.page.html',
  styleUrls: ['./devolver-equipo.page.scss'],
})
export class DevolverEquipoPage implements OnInit {
  constructor(
    private alertCtrl: AlertaService,
    private spinner: SpinnerService,
    private platform: Platform,
    private equipoService: EquiposService,
    private auth: AuthService,
    private photoService: PhotoService
  ) {}
  plat;
  DevolverEquipoModal: boolean;
  lista: Array<any> = [];
  sesion;
  equipo;
  cargando: boolean = true;
  url = environment.SERVER_REF;
  Files: File[] = [];

  async ionViewWillEnter() {
    this.cargando = true;
    await this.ngOnInit();
  }
  async ngOnInit() {
    const loading = await this.spinner.presentLoading();
    this.DevolverEquipoModal = false;
    this.equipo = null;
    this.plat = await this.platform.platforms()[0];
    this.sesion = await this.auth.getSesion();
    this.lista = await this.obtenerEquiposAsignados();
    loading.dismiss();
    this.cargando = false;
  }
  async scanQRCode() {
    var equipo = await this.equipoService.scanQRCode(true);
    if (equipo != null) {
      var x = this.lista.find((x) => x.id === equipo.id);
      if (x != null) {
        await this.devolver(equipo);
      }
    }
  }
  async obtenerEquiposAsignados() {
    var res = await this.equipoService.misAsignados(this.sesion.empleadoID);
    await this.obtenerImagenes(res);
    return res;
  }
  async devolver(equipo) {
    await this.alertCtrl.presentAlert(
      '¿Seguro que desea devolver este equipo?',
      '',
      'SI',
      async () => {
        this.equipo = equipo;
        this.DevolverEquipoModal = true;
      }
    );
  }
  cerrarModal() {
    this.DevolverEquipoModal = false;
    this.equipo = null;
    this.Files = [];
    this.photoService.photos = [];
  }
  async obtenerImagenes(equipos) {
    for (let index = 0; index < equipos.length; index++) {
      const equipo = equipos[index];
      equipo.imgs = await this.equipoService.obtenerImagenes(equipo.id);
    }
  }
  formatoNombre(str) {
    return capitalize(str);
  }
  async terminarDevolucion(ID) {
    var res = await this.equipoService.devolverEquipo(ID,this.Files);
    if (res) {
      this.ngOnInit();
    }
  }
  async tomarFoto(e) {
    if (this.plat != 'desktop') {
      var x = await this.photoService.addNewToGallery(e, this.equipo.id, true);
      if (x) {
        this.Files.push(x);
      }
    }
  }
  async onFileSelect(e) {
    if (e.target.files.length > 3) {
      return await this.alertCtrl.presentToast('Solo se permiten 3 imagenes como máximo')
    }
    const array = Array.from(e.target.files);
    var imgAceptada = true;
    array.forEach(async (x: File) => {
      var pesoMegas = this.bytesToSize(x.size)
      if (pesoMegas > 4) {
        return (imgAceptada = false);
      }
    });
    if (!imgAceptada) {
      return await this.alertCtrl.presentToast(
        'Una imagen es demasaido pesada, 4MB permitidos'
      );
    }
    this.Files = await e.target.files;
  }
  puedeTerminarDevolucion(): Boolean {
    return this.Files.length == 0 ? true : false;
  }
   bytesToSize(bytes) {
    var res = bytes / 1000 / 1000 
    return res
  }
}
