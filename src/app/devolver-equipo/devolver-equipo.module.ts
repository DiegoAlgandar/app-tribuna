import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DevolverEquipoPageRoutingModule } from './devolver-equipo-routing.module';

import { DevolverEquipoPage } from './devolver-equipo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DevolverEquipoPageRoutingModule
  ],
  declarations: [DevolverEquipoPage]
})
export class DevolverEquipoPageModule {}
