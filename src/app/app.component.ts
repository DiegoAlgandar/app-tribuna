import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import 'swiper/css/bundle';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {

  public usuarioPages = [//Opciones para Admin
    { title: 'Solicitar Equipo', url: '/Solicitar-equipo', icon: 'newspaper' },
    { title: 'Reportar Incidencia', url: '/Reportes', icon: 'alert-circle' },
    { title: 'Devolver Equipo', url: '/Devolver-equipo', icon: 'return-up-back' }
  ];

  public adminPages = [
    { title: 'Inventario', url: '/Inventario', icon: 'file-tray-stacked' },
    { title: 'Empleados', url: '/Empleados', icon: 'people-circle' },
    { title: 'Administracion', url: '/Administracion', icon: 'id-card' },
    { title: 'Bitacora', url: '/Bitacora', icon: 'bar-chart' }
  ];

  constructor(public nav: Router, private menu: MenuController) { }
  async ionViewWillEnter() {
    console.log('base');
  }
  async ngOnInit() {
    console.log('init');
  }
  async cerrarSesion() {
    await localStorage.removeItem('sesionActive');
    this.nav.navigateByUrl('/Acceder');
  }
  cerrarMenu() {
    this.menu.close()
  }
}
