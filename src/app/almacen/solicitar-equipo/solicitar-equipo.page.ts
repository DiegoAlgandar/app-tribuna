import { Component, OnInit } from '@angular/core';
import { MenuController, ModalController, Platform } from '@ionic/angular';
import { CustomModalPage } from 'src/app/custom-modal/custom-modal.page';
import { capitalize, fecha } from 'src/app/extras/helpers';
import { AlertaService } from 'src/app/services/alertas.service';
import { AuthService, TOKEN_KEY } from 'src/app/services/auth.service';
import { EquiposService } from 'src/app/services/equipos.service';

@Component({
  selector: 'app-solicitar-equipo',
  templateUrl: './solicitar-equipo.page.html',
  styleUrls: ['./solicitar-equipo.page.scss'],
})
export class SolicitarEquipoPage implements OnInit {
  constructor(
    private platform: Platform,
    private modal: ModalController,
    private equipoService: EquiposService,
    private alertaService: AlertaService,
    private menu: MenuController,
    private auth: AuthService
  ) {}
  opt: string = '';
  plat;
  equipo: any;
  lista: Array<any> = [];
  emploeadoID = null;
  ticket = null;
  equiposTicket: Array<any> = [];
  empleado;
  fechaActual: Date = new Date();
  ionViewWillEnter() {
    this.menu.enable(true);
  }
  async ngOnInit() {
    this.plat = this.platform.platforms()[0];
    console.log(this.plat);
    this.emploeadoID = await JSON.parse(localStorage.getItem(TOKEN_KEY))
      .empleadoID;
    if (this.emploeadoID != null) {
      this.empleado = await this.auth.empleadoInfo(this.emploeadoID);
    }
  }
  onChange(a) {
    this.opt = a.target.value;
  }
  fecha() {
    return fecha(this.fechaActual)
  }
  async abrirModal() {
    const modal = await this.modal.create({
      component: CustomModalPage,
      keyboardClose: false,
      id: 'customModal',
      componentProps: {
        data: this.opt,
      },
    });
    modal.onDidDismiss().then(async (x) => {
      if (x.data === undefined || !x.data) {
        return;
      }
      var estaAregado = await this.isAgregado(x?.data.id);
      if (!estaAregado) {
        this.lista.push(x?.data);
      }
    });
    await modal.present();
  }
  async asignarEquipos() {
    this.equiposTicket = await this.lista;
    var res = await this.equipoService.asignarEquipo(
      this.emploeadoID,
      this.lista,
      this.imprimirticket
    );
    if (res) {
      this.ticket = true;
    }
  }
  Remove(id) {
    this.lista = this.lista.filter((x) => x.id != id);
  }
  imprimirticket() {
    console.log('Ticket generado');
    var x = document.getElementById('btn-ticket');
    x.click();
  }
  capitalize(str) {
    return capitalize(str);
  }
  async scanQRCode() {
    var equipo = await this.equipoService.scanQRCode(true);
    if (equipo != null) {
      var estaAregado = await this.isAgregado(equipo.id);
      if (!estaAregado) {
        this.lista.push(equipo);
      }
    }
  }
  async isAgregado(ID) {
    var estaAregado = this.lista.find((equipo) => equipo.id === ID);
    if (estaAregado) {
      await this.alertaService.presentToast('Ya esta agregado a su lista');
      return true;
    }
    return false;
  }
}