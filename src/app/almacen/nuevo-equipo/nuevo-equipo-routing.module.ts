import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NuevoEquipoPage } from './nuevo-equipo.page';

const routes: Routes = [
  {
    path: '',
    component: NuevoEquipoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NuevoEquipoPageRoutingModule {}
