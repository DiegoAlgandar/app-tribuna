import { Component, OnInit } from '@angular/core';
import { ModalController, Platform } from '@ionic/angular';
import { capitalize } from 'src/app/extras/helpers';
import { Equipo } from 'src/app/models/Equipo';
import { EquiposService } from 'src/app/services/equipos.service';
import { PhotoService } from 'src/app/services/photo.service';
import { ListadoPage } from 'src/app/usuarios/listado/listado.page';

@Component({
  selector: 'app-nuevo-equipo',
  templateUrl: './nuevo-equipo.page.html',
  styleUrls: ['./nuevo-equipo.page.scss'],
})
export class NuevoEquipoPage implements OnInit {
  newEquipo: Equipo;
  constructor(
    private modal: ModalController,
    private photoService: PhotoService,
    private platform: Platform,
    private equipoService: EquiposService
  ) {
    this.newEquipo = new Equipo();
  }
  plat;
  tipo_Equipo: string = '';
  usuario;
  tipo_especifico: string = '';
  Files: File[] = [];
  ID = null;
  asignado: string = '';
  modalImagenes: boolean = false;
  async ngOnInit() {
    this.modalImagenes = false;
    this.plat = await this.platform.platforms()[0];
    this.usuario = false;
  }
  onChange(e) {
    this.tipo_Equipo = e.target.value;
    console.log(this.tipo_Equipo);
  }
  async onSubmit(e) {
    e.preventDefault();
    if (this.newEquipo.Disco_Duro) {
      this.newEquipo.Disco_Duro = parseInt(
        this.newEquipo.Disco_Duro.toString()
      );
    }
    if (this.tipo_Equipo === 'chip') {
      this.newEquipo.Nombre_Equipo = 'Chip';
      this.newEquipo.Tipo = 'Chip';
    }
    this.newEquipo.Tipo = await capitalize(this.newEquipo.Tipo)
    var res = await this.equipoService.nuevoEquipo(this.newEquipo);
    console.log(res.id);
    this.ID = res.id;
    if (this.ID) this.imagenesModal();
  }
  async onFileSelect(e) {
    this.Files = [];
    this.Files = await e.target.files;
  }
  async subir() {
    console.log(this.newEquipo);
    const subidas = await this.equipoService.subir(this.Files, this.ID);
    if (subidas) this.limpiar();
  }
  async asignarEquipo(e) {
    console.log(e.target.value);
    if (e.target.value === 'Asignado') {
      var modal = await this.modal.create({
        component: ListadoPage,
        keyboardClose: false,
        backdropDismiss: false,
        id: 'Listado',
      });
      modal.onDidDismiss().then((x) => {
        this.asignado = x.data?.Nombre;
        this.newEquipo.Usuario_Actual = x.data?.ID;
        console.log(this.asignado);
        !this.asignado ? (this.newEquipo.Estado = '') : null;
      });
      return await modal.present();
    } else this.asignado = '';
  }
  async takePhoto(e) {
    if (this.plat === 'desktop') {
      console.log('no disponible');
      return;
    }
    await this.photoService.addNewToGallery(e, this.ID);
  }
  async imagenesModal() {
    this.modalImagenes = !this.modalImagenes;
  }
  async cerrarModal() {
    await this.modal.dismiss({ id: 'ImagenesModal' });
    this.limpiar();
  }
  limpiar() {
    this.newEquipo = new Equipo();
    this.Files = [];
    this.ID = null;
    this.modalImagenes = false;
  }
}
