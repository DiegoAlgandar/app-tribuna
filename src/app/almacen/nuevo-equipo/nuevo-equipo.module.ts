import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NuevoEquipoPageRoutingModule } from './nuevo-equipo-routing.module';

import { NuevoEquipoPage } from './nuevo-equipo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NuevoEquipoPageRoutingModule
  ],
  declarations: [NuevoEquipoPage]
})
export class NuevoEquipoPageModule {}
