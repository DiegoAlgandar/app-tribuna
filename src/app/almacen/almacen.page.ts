import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInput, NavController, Platform } from '@ionic/angular';
import { SpinnerService } from '../services/spinner.service';
import { Router } from '@angular/router';
import { EquiposService } from '../services/equipos.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-almacen',
  templateUrl: './almacen.page.html',
  styleUrls: ['./almacen.page.scss'],
})
export class AlmacenPage implements OnInit {
  @ViewChild('autofocus', { static: false }) searchbar: IonInput;
  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    private service: SpinnerService,
    private router: Router,
    private equipoService: EquiposService
  ) { }
  searchFilter: any;
  plat: any;
  filteredarray: Array<any> = [];
  inventario: Array<any> = [];
  showSpinner: Boolean = false;
  buscando = false
  url = environment.SERVER_REF
  async ionViewWillEnter() {
    this.showSpinner = true;
    this.buscando = false;
    this.searchFilter = '';
    await this.ngOnInit()
  }
  async ngOnInit() {
    const loading = await this.service.presentLoading();
    try {
      this.showSpinner = true;
      this.buscando = false;
      this.searchFilter = '';
      this.plat = await this.platform.platforms()[0];
      if (this.plat === 'desktop') {
        document.getElementById('scanQRCode').style.display = 'none';
      }
      var res = await this.equipoService.obtenerEquipos()
      if (res) {
        this.inventario = res
        this.filteredarray = await this.equipoService.filtrarResultados()
      }
      console.log(this.inventario);
      this.showSpinner = false
    } catch (error) {
      console.log(error);
    }
    await loading.dismiss();
  }
  async scanQRCode() {
    this.equipoService.scanQRCode()
  }
  OnSearch(e) {
    this.searchFilter = e.target.value;
    this.filteredarray = this.inventario.filter(
      (x) => x.nombre_Equipo?.toLowerCase()
        .includes(this.searchFilter.toLocaleLowerCase().trim())
    );
  }
  async doRefresh(e) {
    await this.ngOnInit();
    e.target.complete();
  }
  formatear(date: Date) {
    return date.toLocaleDateString();
  }
  async navegar(ID) {
    return await this.router.navigateByUrl(`/Inventario/detalle-equipo/${ID}`)
  }
  async buscar() {
    this.buscando = !this.buscando
    if (this.buscando) {
      setTimeout(() => this.searchbar.setFocus(), 300);
    } else {
      this.searchFilter = ''
      if (!this.searchFilter) {
        this.filteredarray = await this.equipoService.filtrarResultados()
      }
    }
  }
}
