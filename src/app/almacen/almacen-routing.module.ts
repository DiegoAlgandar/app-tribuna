import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AlmacenPage } from './almacen.page';

const routes: Routes = [
  {
    path: '',
    component: AlmacenPage
  },
  {
    path: 'nuevo-equipo',
    loadChildren: () => import('./nuevo-equipo/nuevo-equipo.module').then( m => m.NuevoEquipoPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AlmacenPageRoutingModule {}
