import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import AxiosClient from '../extras/axiosClient';
import { Router } from '@angular/router';
import { AlertaService } from './alertas.service';
export const TOKEN_KEY = 'sesionActive';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private storage: StorageService, private router: Router,
    private alertCtrl: AlertaService
  ) { }
  async login(loginModel: any, loading: HTMLIonLoadingElement) {
    try {
      const res = await AxiosClient.post(`/Usuarios/IniciarSesion`, loginModel);
      localStorage.setItem(TOKEN_KEY, JSON.stringify(res.data));
      res.data.rol === 'Admin' ? this.router.navigateByUrl('/Bitacora') : this.router.navigateByUrl('/Solicitar-equipo')
      await loading.dismiss();
      return res.data;
    } catch (error) {
      await loading.dismiss();
      await this.alertCtrl.presentToast(error.response?.data ? error.response.data : error.message)
    }
  }
  async logout() {
    return await localStorage.removeItem(TOKEN_KEY);
  }
  getSesion() {
    var sesion = localStorage.getItem(TOKEN_KEY)
    return sesion ? JSON.parse(sesion) : null
  }
  async nuevoEmpleado(newEmpleado, loading: HTMLIonLoadingElement) {
    try {
      const res = await AxiosClient.post('Empleados', newEmpleado, {
        headers: { 'Content-Type': 'application/json' },
      });
      await this.alertCtrl.presentToast(res.data)
      await loading.dismiss();
      return res.data;
    } catch (error) {
      console.log(error);
      await loading.dismiss();
      return this.alertCtrl.presentToast(error.message);
    }
  }
  async eliminarEmpleado(ID) {
    try {
      var x = await AxiosClient.delete(`Empleados/${ID}`);
      await this.alertCtrl.presentToast(x.data)
      return x.data;
    } catch (error) {
      console.log(error.message);
    }
  }
  async obtenerEmpleados(page) {
    var lista: Array<any> = [];
    try {
      //TokenAuth(this.token);
      const res = await AxiosClient.get(`Empleados/Paginacion/${page}`);
      lista = res.data;
      return lista;
    } catch (error) {
      console.log(error.message);
      await this.alertCtrl.presentToast(error.message)
      return lista
    }
  }
  setPage(type, page: number) {
    if (type === 'Next') {
      return (page += 15);
    } else if (type == 'Prev') {
      if (page === 0) {
        console.log('No Consulta');
        return;
      }
      return (page -= 15);
    }
  }
  async burcarEmpleado(nombre) {
    try {
      var lista: Array<any> = [];
      if (nombre.trim() === '') {
        console.log('done');
        return (lista = await this.obtenerEmpleados(0));
      }
      lista = (await AxiosClient.get(`Empleados/${nombre}`)).data;
      return lista;
    } catch (error) {
      console.log(error.message);
    }
  }
  async nuevoUsuario(body, loading: HTMLIonLoadingElement) {
    try {
      var res = await AxiosClient.post('Usuarios/Nuevo_Usuario', body, {
        headers: { 'Content-Type': 'application/json' },
      });

      if (res.data === -2) {
        await loading.dismiss();
        return await this.alertCtrl.presentAlert(
          'Nombre de usuario repetido',
          '¡Por favor elija otro nombre de usuario para su cuenta y vuelva a enviar el formulario!'
        );
      }
      await this.alertCtrl.presentAlert(
        'Usuario Registrado',
        'Espera a que esta cuenta sea autorizada por el administrador en el area de sistemas.',
        'Acceder',
        () => this.router.navigateByUrl('/Acceder')
      );
      await loading.dismiss();
      return res.data
    } catch (error) {
      console.log(error.message);
      await loading.dismiss();
      await this.alertCtrl.presentToast(error.message);
    }
  }
  sesionExpirada() {
    var x = JSON.parse(localStorage.getItem('sesionActive'))
    var expiracion = new Date(x.expires).getTime()
    var fechaActual = Date.now()
    //console.log(new Date(expiracion));
    //console.log(new Date(fechaActual));

    if (fechaActual > expiracion) {
      return true
    } else return false
  }
  async empleadoInfo(emploeadoID) {
    try {
      return (await AxiosClient.get(`Empleados/Consulta/${emploeadoID}`)).data
    } catch (error) {
      console.log(error);
    }
  }
}
