import { Injectable } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class AlertaService {
  constructor(
    private alertCrtl: AlertController,
    private toast: ToastController
  ) {}

  async presentAlert(header: string, message: string, btn?, callback?) {
    const alert = await this.alertCrtl.create({
      cssClass: 'my-custom-class',
      keyboardClose: false,
      backdropDismiss: false,
      header: header,
      message: message,
      buttons: btn
        ? [
            {
              text: btn,
              handler: callback ? callback : null,
            },
            btn === 'SI' ? 'NO' : 'Ok',
          ]
        : ['Ok'],
    });
    await alert.present();
  }
  async presentToast(header: string, btn?, callback?) {
    const toast = await this.toast.create({
      header,
      cssClass: 'customToast',
      buttons: btn
        ? [
            {
              text: btn,
              handler: callback ? callback : null,
            },
          ]
        : ['Ok'],
    });
    await toast.present();
    setTimeout(() => {
      toast.dismiss();
    }, 4500);
  }
}