import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BarcodeScanner } from '@awesome-cordova-plugins/barcode-scanner/ngx';
import { ModalController } from '@ionic/angular';
import AxiosClient from '../extras/axiosClient';
import { AlertaService } from './alertas.service';
import { SpinnerService } from './spinner.service';

@Injectable({
  providedIn: 'root',
})
export class EquiposService {
  constructor(
    private barcodeScanner: BarcodeScanner,
    private router: Router,
    private modal: ModalController,
    private spinnerCtrl: SpinnerService,
    private alerta: AlertaService
  ) {}
  inventario: Array<any> = [];
  filteredarray: Array<any> = [];
  Files: File[] = [];
  async misAsignados(empleadoID) {
    try {
      return (
        await AxiosClient.get(`Equipos_inventario/Asignados/${empleadoID}`)
      ).data;
    } catch (error) {
      return error;
    }
  }
  async obtenerEquipos() {
    try {
      var resp = await AxiosClient.get('Equipos_inventario');
      await (this.inventario = resp.data);
      for (let index = 0; index < this.inventario.length; index++) {
        const element = resp.data[index];
        element.imgs = await this.obtenerImagenes(element.id);
      }
      return this.inventario;
    } catch (error) {
      await this.alerta.presentToast(
        error.message === 'Network Error'
          ? 'Error de red, verifique su conexión'
          : error.message
      );
      console.log(error);
    }
  }
  async equipoInfo(ID) {
    const loading = await this.spinnerCtrl.presentLoading();
    try {
      var res = (await AxiosClient.get(`Equipos_inventario/${ID}`)).data;
      if (!res) {
        await this.alerta.presentToast('No se encontró el equipo solicitado');
        return null;
      }
      const resp = await AxiosClient.get(`img/Consulta/${res.id}`);
      res.imagenes = await resp.data;
      loading.dismiss();
      return res;
    } catch (error) {
      console.log(error);
      await this.alerta.presentToast(error.message);
      loading.dismiss();
      return null;
    }
  }
  async obtenerImagenes(ID) {
    try {
      var res = await AxiosClient.get(`Img/Consulta/${ID}`);
      return res.data || [];
    } catch (error) {
      console.log(error);
    }
  }
  async filtrarResultados() {
    await this.inventario.forEach((e) => {
      e.fecha_Registro = new Date(e.fecha_Registro);
    });

    this.filteredarray = await this.inventario.sort(
      (a, b) => b.fecha_Registro.getTime() - a.fecha_Registro.getTime()
    );
    return this.filteredarray;
  }
  async scanQRCode(accion?: boolean): Promise<any> {
    // accion indica una solicitud o devolucion
    this.barcodeScanner
      .scan()
      .then(async (barcodeData) => {
        if (barcodeData) {
          if (accion) {
            return await this.equipoInfo(barcodeData.text);
          }
          return await this.router.navigateByUrl(
            `Inventario/detalle-equipo/${barcodeData.text}`
          );
        }
      })
      .catch(async (err) => {
        console.log('Error', err);
        return await this.alerta.presentToast(`${err}`);
      });
  }
  async subir(files, ID) {
    if (files.length > 3) {
      return await this.alerta.presentToast(
        'Se permiten 3 imagenes como máximo, vuelva a seleccionar sus imagenes '
      );
    }
    if (files.length == 0) {
      return await this.alerta.presentToast(
        'No se ha seleccionado ninguna imagen'
      );
    }
    const array = Array.from(files);
    var imgAceptada = true;
    array.forEach(async (x: File) => {
      var pesoMegas = this.bytesToSize(x.size)
      if (pesoMegas > 4) {
        return (imgAceptada = false);
      }
    });
    if (!imgAceptada) {
      return await this.alerta.presentToast(
        'Una imagen es demasaido pesada, 4MB permitidos'
      );
    }
    var formData = new FormData();
    formData.append('ID', ID);
    for (let index = 0; index < files.length; index++) {
      const element = files[index];
      formData.append('Imagen', element);
    }
    try {
      const res = await AxiosClient.post('img', formData);
      console.log(res.data);
      await this.alerta.presentToast(`Imagenes asignadas a al equipo ${ID}`);
      await this.modal.dismiss({ id: 'ImagenesModal' });
      this.router.navigateByUrl('/Inventario');
      return true;
    } catch (err) {
      console.log(err);
    }
  }
  async nuevoEquipo(equipo) {
    console.log(equipo);
    try {
      //equipo.Chip = Guid_Empty;
      var res = await AxiosClient.post(`/Equipos_inventario`, equipo, {
        headers: { 'Content-Type': 'application/json' },
      });
      console.log(res.data);
      var msj = ' ha sido dado de alta con exito.';
      await this.alerta.presentToast(
        equipo.Tipo != '' ? `${equipo.Tipo}` + msj : 'chip' + msj
      );
      return res.data;
    } catch (error) {
      if (error.message === 'Network Error') {
        return await this.alerta.presentAlert(
          `Error 500`,
          'Error de red en servidor, verifique su conexion',
          null,
          null
        );
      }
      await this.alerta.presentAlert(
        `¡Error ${error.response.status}!`,
        `${error.response.data.message}`,
        null,
        null
      );
    }
  }
  async obtenerEquiposLibres(tipo) {
    var lista: Array<any> = [];
    try {
      var res = await AxiosClient.get(`Equipos_inventario/${tipo}/Libres`);
      lista = res.data;
    } catch (error) {
      console.log(error.response.data);
      console.log(error.response.status);
    }

    return lista;
  }
  async asignarEquipo(emploeadoID, lista, fn) {
    try {
      for (let index = 0; index < lista.length; index++) {
        const equipo = lista[index];
        var resp = await AxiosClient.put('Equipos_inventario/Asignar_Equipo', {
          equipoID: equipo.id,
          emploeadoID: emploeadoID,
        });
        console.log(resp.data);
      }

      await this.alerta.presentAlert(
        'Solicitud completada',
        'Su solicitud se completo con exito, proceda a presentar el ticket en area de sistemas para su firma y sellado',
        'Imprimir ticket',
        () => fn()
      );
      return resp.data;
    } catch (error) {
      console.log(error);
    }
  }
  async devolverEquipo(ID,files) {
    try {
      //borrar imagenes
      var imagenes = await AxiosClient.delete(`img/${ID}`);
      console.log(imagenes.data);
      // nuevas imagenes
      var formData = new FormData();
      formData.append('ID', ID);
      for (let index = 0; index < files.length; index++) {
        const element = files[index];
        formData.append('Imagen', element);
      }
      const neuvas = await AxiosClient.post('img', formData);
      console.log(neuvas.data);
      //cambiar estado
      var res = await AxiosClient.put(
        `Equipos_inventario/Devolver_equipo/${ID}`
      );
      if (res.data != null) {
        this.alerta.presentToast(res.data);
      }
      return res.data;
    } catch (error) {
      console.log(error);
    }
  }
  bytesToSize(bytes) {
    var res = bytes / 1000 / 1000 
    return res
  }
}
