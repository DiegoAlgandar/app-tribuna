import { Injectable } from '@angular/core';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import AxiosClient from '../extras/axiosClient';
import { UserPhoto } from '../models/Photo';
import { AlertaService } from './alertas.service';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  foto: File
  public photos: UserPhoto[] = [];
  constructor(private alerta:AlertaService) {}
  private async savePicture(photo: Photo,id,devolver?) {
    const fileName = 'm' + new Date().getTime() + `.${photo.format}`;
    const response = await fetch(photo.webPath!);
    const blob = await response.blob();
    this.foto = await new File([blob],fileName)
    if (devolver) {
      return this.foto
    } 
    const formData = await new FormData();
    formData.append('ID', id);
    formData.append('Imagen', this.foto);    
    try {
      const res = await AxiosClient.post('img', formData);
      console.log(res.data);
    } catch (error) {
      console.log(error);
    }
  }

  public async addNewToGallery(e,id,devolver?) {
    const capturedPhoto = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality: 100
    }); 

    if (this.photos.length < 3) {
      this.photos.unshift({
        filepath: `img-${this.photos.length + 1}`,
        webviewPath: capturedPhoto.webPath
      });
    }else return this.alerta.presentToast('No puede añadir mas de 3 imagenes')
    
    var p = await this.savePicture(capturedPhoto,id,devolver);
    return p
  }
}
