import { Component, OnInit } from '@angular/core';
import { ModalController, Platform, ToastController } from '@ionic/angular';
import { NgxQrcodeElementTypes } from '@techiediaries/ngx-qrcode';

@Component({
  selector: 'app-qr-modal',
  templateUrl: './qr-modal.page.html',
  styleUrls: ['./qr-modal.page.scss'],
})
export class QrModalPage implements OnInit {
  constructor(
    private platform: Platform,
    private modal: ModalController,
    private toast: ToastController
  ) { }
  QR;
  elementType = NgxQrcodeElementTypes.CANVAS;
  data;
  descargado: boolean = false;
  desc = {
    valor: false,
  };
  plat;
  async ngOnInit() {
    //JSON.parse(localStorage.getItem(`descarga-${this.data}`));
    this.desc.valor =
      (await JSON.parse(localStorage.getItem(`descargado`))?.valor) || false;
    this.plat = await this.platform.platforms()[0];
    console.log(this.data);
    await this.generarQR();
  }
  async cerrarModal() {
    this.modal.dismiss({ id: 'Qr_Generado' });
  }
  async generarQR() {
    return (this.QR = await `${this.data.id}`);
  }
  async descargarQR() {
    this.desc.valor = true;
    let toast = await this.toast.create({
      header: 'El QR se descargará como imagen, ¿Deseas continuar?',
      buttons: [
        {
          text: 'Si',
          handler: () => {
            this.descargado = true;
            const canvas = document.querySelector(
              'canvas'
            ) as HTMLCanvasElement;
            const imgData = canvas.toDataURL('image/jpeg').toString();
            console.log(imgData);
            var link = document.createElement('a');
            link.download = `${this.data.nombre}-${this.data.id}`;
            link.href = imgData;
            link.click();
            link.remove()
            setTimeout(() => {
              toast.dismiss();
            }, 1000);
          },
        },
        {
          text: 'No',
          handler: () => {
            this.descargado = false;
            toast.dismiss();
          },
        },
      ],
    });
    toast.present();
  }
}
