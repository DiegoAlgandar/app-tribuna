import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AlertaService } from '../services/alertas.service';
import { AuthService, TOKEN_KEY } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class IngresadoGuard implements CanActivate {
  constructor(public nav: Router, private auth: AuthService, private alertCtrl:AlertaService) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (localStorage.getItem(TOKEN_KEY)) {
      var x = this.auth.sesionExpirada()
      //console.log(x);
      if (x) {
        localStorage.removeItem(TOKEN_KEY)
        this.alertCtrl.presentToast('Su sesion ha expirado, vuelva a iniciar sesion')
        this.nav.navigateByUrl('/Acceder')
        console.log('expirada');

      } //console.log('sigue');
      return true
    } else {
      this.nav.navigateByUrl('/Acceder')
      return false
    }
  }

}
