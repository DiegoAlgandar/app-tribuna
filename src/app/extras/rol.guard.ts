import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AlertaService } from '../services/alertas.service';
import { TOKEN_KEY } from '../services/auth.service';

@Injectable({
  providedIn: 'root',
})
export class RolGuard implements CanActivate {
  constructor(public nav: Router, public alertCtrl: AlertaService ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (JSON.parse(localStorage.getItem(TOKEN_KEY)).rol === 'Admin') {
      return true;
    } else {
      this.alertCtrl.presentAlert("No autorizado", "No tiene permisos para ver esta pagina, si cree que es un error comuniquese con el área de sistemas")
      //localStorage.removeItem(TOKEN_KEY);
      //this.nav.navigateByUrl('/Home');
      return false;
    }
  }
}
