import { format, parseISO } from 'date-fns';
import { FormGroup } from '@angular/forms';
export const PLAZAS = [
  //Generar crud en API ?
  { id: 1, nombre: 'Obregón' },
  { id: 2, nombre: 'Guaymas' },
  { id: 3, nombre: 'Navojoa' },
  { id: 4, nombre: 'Hermosillo' },
];
export const AREAS = [
  //Generar crud en API ?
  { id: 1, nombre: 'Administración' },
  { id: 2, nombre: 'Publicidad' },
  { id: 3, nombre: 'Circulación' },
  { id: 4, nombre: 'Comercial' },
  { id: 5, nombre: 'Producción' },
  { id: 6, nombre: 'Redacción' },
  { id: 7, nombre: 'Mostrador' },
  { id: 8, nombre: 'RH' },
  { id: 9, nombre: 'Sistemas' },
  { id: 10, nombre: 'Diseño' },
  { id: 11, nombre: 'Vigilancia' },
];
export const CATEGORIAS = [
  //Generar crud en API ?
  { nombre: 'Asesoría' },
  { nombre: 'Celular' },
  { nombre: 'Correo' },
  { nombre: 'Hardware' },
  { nombre: 'Internet' },
  { nombre: 'Licencias' },
  { nombre: 'Parnet' },
  { nombre: 'Red' },
  { nombre: 'Reunión' },
  { nombre: 'Sistemas' },
  { nombre: 'Software' },
  { nombre: 'Usuarios' },
];
export const Guid_Empty = '00000000-0000-0000-0000-000000000000';

export function capitalize(str) {
  var splitStr = str.toLowerCase().split(' ');
  for (var i = 0; i < splitStr.length; i++) {
    splitStr[i] =
      splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  return splitStr.join(' ');
}
export function formatDate(value: string) {
  var fecha = format(parseISO(value), 'dd/MM/yyyy');
  console.log(fecha);
  return fecha;
}
export function confirmedValidator(
  controlName: string,
  matchingControlName: string
) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];
    if (matchingControl.errors && !matchingControl.errors.confirmedValidator) {
      return;
    }
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ confirmedValidator: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
}
export function fecha(fechaActual) {
  return `${(fechaActual.getMonth() + 1)
    .toString()
    .padStart(2, '0')}/${fechaActual
    .getDate()
    .toString()
    .padStart(2, '0')}/${fechaActual
    .getFullYear()
    .toString()
    .padStart(4, '0')} ${fechaActual
    .getHours()
    .toString()
    .padStart(2, '0')}:${fechaActual
    .getMinutes()
    .toString()
    .padStart(2, '0')}:${fechaActual
    .getSeconds()
    .toString()
    .padStart(2, '0')}`;
}
