import axios from "axios";
import { environment } from "src/environments/environment";

const AxiosClient= axios.create({
    //baseURL: 'http://192.168.1.49:5000/api/' 
    baseURL: environment.API
    /*process.env.backendURL*/
})
export default AxiosClient
