export class Equipo {
    Office: string = ""
    Usuario_Actual: string = null
    Estado: string = ""
    Procesador: string = ""
    SO: string = ""
    Disco_Duro: number=0
    Nombre_Equipo: string = ""
    Marca:string = ""
    Modelo: string = ""
    Tipo: string = ""
    N_Serie: string = ""
    Chip:string = ""
    Plan:string = ""
    Cuenta:string = ""
    Cuenta_Padre:string = ""
    Region:string = ""
    Razon_Social:string = ""
    N_Telefono:string =""
}
    