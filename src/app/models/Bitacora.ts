export class Bitacora {
  UsuarioID :string =""
  Estado : string = ""
  Area  : string  = ""
  Plaza : string = ""
  Tipo : string = ""
  Problema : string = ""
  Prioridad : string = ""
  Categoria : string = ""
  Solucion : string = ""
  Tiempo_Invertido : number = 0
  Responsable : string = ""
}
