export class Usuario {
  ID: string = ""
  NombreUsuario: string = ""
  Nombre: string = ""
  Apellido_Paterno: string = ""
  Apellido_Materno: string = ""
  Autorizado: boolean= false
  Contrasenia: string = ""
  Rol: string = ""
  Registro: Date= null
}