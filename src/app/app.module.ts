import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { IonicStorageModule } from '@ionic/storage-angular';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BarcodeScanner } from '@awesome-cordova-plugins/barcode-scanner/ngx';
import { HttpClientModule } from '@angular/common/http';
import {SwiperModule} from 'swiper/angular';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import {NgxPrintModule} from 'ngx-print';


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [NgxPrintModule,BrowserModule, NgxQRCodeModule, SwiperModule,FormsModule,
     ReactiveFormsModule, IonicModule.forRoot(), AppRoutingModule, FormsModule,
      HttpClientModule, IonicStorageModule.forRoot()],
  providers: [BarcodeScanner, { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {}

